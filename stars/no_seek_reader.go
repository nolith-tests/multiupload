package stars

import (
	"io"
)

type noSeekReader struct {
	size int64
	cnt  int64
}

func (n *noSeekReader) Read(p []byte) (int, error) {
	if n.cnt >= n.size {
		return 0, io.EOF
	}

	var i int
	for i = 0; i < len(p) && int64(i)+n.cnt < n.size; i++ {
		p[i] = '*'
	}
	n.cnt += int64(i)

	return i, nil
}

// New returns an io.reader that reads stars ('*')
// It returns size bytes of '*'
func New(size int64) io.Reader {
	return &noSeekReader{size: size}
}

// MegaStreams is an utility around `New` for expressing size in MB
func MegaStream(size float64) io.Reader {
	return New(int64(size * 1024 * 1024))
}

// GigaStreams is an utility around `New` for expressing size in GB
func GigaStream(size float64) io.Reader {
	return MegaStream(size * 1024)
}
