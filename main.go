package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/minio/minio-go"

	"gitlab.com/nolith-tests/multiupload/splitter"
	"gitlab.com/nolith-tests/multiupload/stars"
)

const (
	uploadAWS     = "aws"
	uploadChunked = "chunked"
	uploadMinio   = "minio"
	uploadSplit   = "split"
	uploadAll     = "all"
)

var uploaders = [4]string{uploadAWS, uploadMinio, uploadChunked, uploadSplit}

var uploadMethod = flag.String("method", "all", "how to perform the upload: [minio, aws, chunked, split]")
var uploadMega = flag.Float64("size", 10, "The size of the upload in MBi.")
var uploadKey = flag.String("key", "multiuopload.test", "the key of the uploaded object")
var uploadBucket = flag.String("bucket", "", "the bucket to upload into. If provided the bucket will not be created")
var debug = flag.Bool("v", false, "debug mode - prints out lots of stuffs")

type Provider struct {
	Endpoint        string
	AccessKeyID     string
	SecretAccessKey string
	Region          string
	SSL             bool
}

type Config map[string]*Provider

func timestampedName(name string) string {
	return fmt.Sprintf("%d-%s", time.Now().Unix(), name)
}

func presignPutObject(bucketName, key string, config *Provider) (string, error) {
	expiry := time.Second * 60 * 60 // 1 hour.

	// using V2 signatures for DO compatibility
	signer, err := minio.NewV2(config.Endpoint, config.AccessKeyID, config.SecretAccessKey, config.SSL)
	if err != nil {
		return "", fmt.Errorf("Can't create minio client. %s", err)
	}

	url, err := signer.PresignedPutObject(bucketName, key, expiry)
	if err != nil {
		return "", fmt.Errorf("Can't generate pre-signed URL. %s", err)
	}

	return url.String(), nil
}

func chunkedTestUpload(stream io.Reader, bucketName, key string, config *Provider) error {
	url, err := presignPutObject(bucketName, key, config)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPut, url, stream)
	if err != nil {
		return fmt.Errorf("Can't crete request. %s", err)
	}

	if *debug {
		dump, err := httputil.DumpRequestOut(req, false)
		if err != nil {
			return fmt.Errorf("Can't dump request. %s", err)
		}
		log.Println(string(dump))
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("Can't perform request. %s", err)
	}

	if resp.StatusCode >= 300 {
		return fmt.Errorf("Upload failed. Status: %s", resp.Status)
	}

	return nil
}

func minioTestUpload(stream io.Reader, bucketName, key string, config *Provider) error {
	client, err := minio.New(config.Endpoint, config.AccessKeyID, config.SecretAccessKey, config.SSL)
	if err != nil {
		return fmt.Errorf("Can't create minio client. %s", err)
	}

	if *debug {
		client.TraceOn(nil)
	}

	if _, err = client.PutObject(bucketName, key, stream, -1, minio.PutObjectOptions{}); err != nil {
		return fmt.Errorf("Can't PutObject. %s", err)
	}

	return nil
}

func awsTestUpload(stream io.Reader, bucketName, key string, config *Provider) error {
	sess, err := session.NewSession(&aws.Config{
		Region:           aws.String(config.Region),
		Credentials:      credentials.NewStaticCredentials(config.AccessKeyID, config.SecretAccessKey, ""),
		Endpoint:         aws.String(config.Endpoint),
		S3ForcePathStyle: aws.Bool(true),
	})

	if err != nil {
		return fmt.Errorf("Can't create AWS session. %s", err)
	}

	if *debug {
		sess.Handlers.Send.PushFront(func(r *request.Request) {
			// Log every request made and its payload
			log.Println("Request:", config.Endpoint, r.Operation.Name, r.Params)
		})
	}

	uploader := s3manager.NewUploader(sess, func(u *s3manager.Uploader) {
		u.PartSize = 10 << 20 // 10MB
	})

	_, err = uploader.UploadWithContext(context.Background(), &s3manager.UploadInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(key),
		Body:   stream,
	})
	if err != nil {
		return fmt.Errorf("Can't Upload Object. %s", err)
	}

	return nil
}

func splitTestUpload(stream io.Reader, bucketName, key string, config *Provider) error {
	partKey := func(n int) *string {
		return aws.String(fmt.Sprintf("%s-part%d", key, n))
	}

	sess, err := session.NewSession(&aws.Config{
		Region:           aws.String(config.Region),
		Credentials:      credentials.NewStaticCredentials(config.AccessKeyID, config.SecretAccessKey, ""),
		Endpoint:         aws.String(config.Endpoint),
		S3ForcePathStyle: aws.Bool(true),
	})

	if err != nil {
		return fmt.Errorf("Can't create AWS session. %s", err)
	}

	if *debug {
		sess.Config.LogLevel = aws.LogLevel(aws.LogDebugWithHTTPBody)
	}

	svc := s3.New(sess)

	reader := splitter.Splitter{
		Src: stream,
		N:   10 << 20, // 10 MB
	}

	ctx, files := reader.Start(context.Background())
	defer reader.Cancel()

	part := 1
	for path := range files {
		select {
		case <-ctx.Done():
			//aborted
			return fmt.Errorf("Operation aborted. %s", ctx.Err())
		default:
			url, err := presignPutObject(bucketName, *partKey(part), config)
			if err != nil {
				return fmt.Errorf("Can't generate presigned URL. %s", err)
			}

			file, err := os.Open(path)
			req, err := http.NewRequest(http.MethodPut, url, file)
			if err != nil {
				return fmt.Errorf("Can't crete request. %s", err)
			}
			defer os.Remove(path)
			fi, err := file.Stat()
			if err != nil {
				return fmt.Errorf("Can't stat file. %s", err)
			}
			req.ContentLength = fi.Size()

			if *debug {
				dump, err := httputil.DumpRequestOut(req, false)
				if err != nil {
					log.Println("Can't dump request. %s", err)
				} else {
					log.Println(string(dump))
				}
			}

			client := http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				return fmt.Errorf("Can't perform request. %s", err)
			}
			defer resp.Body.Close()

			io.Copy(ioutil.Discard, resp.Body)

			log.Println("Uploaded", *partKey(part))

			if resp.StatusCode >= 300 {
				return fmt.Errorf("Pre-signed URL Upload failed. Status: %s", resp.Status)
			}

			defer func(n int) {
				_, err := svc.DeleteObject(&s3.DeleteObjectInput{
					Bucket: aws.String(bucketName),
					Key:    partKey(n),
				})
				if err != nil {
					log.Println("Can't DeleteObject for part", n, err)
				}
			}(part)

			part++
		}
	}

	if part <= 1 {
		return nil
	}

	parts := make([]*s3.CompletedPart, part-1)

	multi, err := svc.CreateMultipartUploadWithContext(ctx, &s3.CreateMultipartUploadInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(key),
	})
	if err != nil {
		return fmt.Errorf("Failed to create multipart upload. %s", err)
	}
	uploadId := *multi.UploadId

	for i := 0; i < part-1; i++ {
		partNumber := int64(i + 1)
		resp, err := svc.UploadPartCopyWithContext(ctx, &s3.UploadPartCopyInput{
			Bucket:     multi.Bucket,
			Key:        aws.String(key),
			PartNumber: &partNumber,
			CopySource: aws.String(fmt.Sprintf("%s/%s", bucketName, *partKey(i + 1))),
			UploadId:   &uploadId,
		})
		if err != nil {
			return fmt.Errorf("Can't UploadPartCopy. %s", err)
		}

		parts[i] = &s3.CompletedPart{ETag: resp.CopyPartResult.ETag, PartNumber: &partNumber}
	}

	_, err = svc.CompleteMultipartUploadWithContext(ctx, &s3.CompleteMultipartUploadInput{
		Bucket:          multi.Bucket,
		Key:             aws.String(key),
		UploadId:        &uploadId,
		MultipartUpload: &s3.CompletedMultipartUpload{Parts: parts},
	})
	if err != nil {
		return fmt.Errorf("Can't complete multipart upload. %s", err)
	}

	return nil
}

func testUpload(key, uploader string, sizeMBi float64, params *Provider) {
	skipBucket := *uploadBucket != ""
	bucket := *uploadBucket

	client, err := minio.New(params.Endpoint, params.AccessKeyID, params.SecretAccessKey, params.SSL)
	if err != nil {
		log.Println("Can't create minio client", err)
		return
	}

	if !skipBucket {
		bucket = timestampedName(fmt.Sprintf("%s-test-%d", uploader, int64(sizeMBi)))

		if err := client.MakeBucket(bucket, params.Region); err != nil {
			log.Println("Can't create bucket", err)
			return
		}
		defer func() {
			if err := client.RemoveBucket(bucket); err != nil {
				log.Println("Can't delete bucket", err)
				return
			}
			log.Println("bucket deleted")
		}()
	}
	log.Println("Bucket", bucket)

	stream := stars.MegaStream(sizeMBi)
	err = nil
	switch uploader {
	case uploadMinio:
		err = minioTestUpload(stream, bucket, key, params)
	case uploadAWS:
		err = awsTestUpload(stream, bucket, key, params)
	case uploadChunked:
		err = chunkedTestUpload(stream, bucket, key, params)
	case uploadSplit:
		err = splitTestUpload(stream, bucket, key, params)
	default:
		log.Println("Unknown upload method", uploader)
		return
	}

	if err != nil {
		log.Println("Can't upload", err)
		return
	}

	oi, err := client.StatObject(bucket, key, minio.StatObjectOptions{})
	if err != nil {
		log.Println("Can't StatObject", err)
		return
	}
	log.Println("Object Size", oi.Size/(1024*1024.0), "MBi")

	err = client.RemoveObject(bucket, key)
	if err != nil {
		log.Println("Can't RemoveObject", err)
		return
	}
}

func main() {
	var conf Config
	flag.Parse()

	confFile, err := ioutil.ReadFile("./config.json")
	if err != nil {
		log.Println("Can't open config file", err)
		return
	}

	if err := json.Unmarshal(confFile, &conf); err != nil {
		log.Println("Can't read config", err)
		return
	}

	for name, params := range conf {
		log.SetPrefix("")
		log.Println("*********************** Testing", name, "***********************")

		for _, uploader := range uploaders {
			if *uploadMethod == uploadAll || *uploadMethod == uploader {
				log.SetPrefix(fmt.Sprintf("%s|%s|", name, uploader))

				testUpload(*uploadKey, uploader, *uploadMega, params)
			}
		}
	}
}
