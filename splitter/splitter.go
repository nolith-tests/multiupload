package splitter

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"runtime"
)

// Splitter
type Splitter struct {
	Src    io.Reader
	N      int64
	Prefix string
	Path   string

	cancel context.CancelFunc
}

func (s *Splitter) Cancel() {
	if s.cancel != nil {
		s.cancel()
	}
}

func (s *Splitter) Start(ctx context.Context) (context.Context, <-chan string) {
	if s.Prefix == "" {
		s.Prefix = fmt.Sprintf("splitter-%d-", s.N)
	}

	files := make(chan string, runtime.NumCPU())

	splitCtx, cancel := context.WithCancel(ctx)
	s.cancel = cancel

	go func() {
		defer close(files)

		for {
			select {
			case <-ctx.Done():
				if err := ctx.Err(); err != nil {
					log.Println("Split aborted.", err)
					return
				}

			default:
				n, path, err := s.split(splitCtx)
				if err != nil {
					log.Println("Error splitting stream.", err)
					return
				}
				if n == 0 {
					return
				}
				files <- path
			}
		}
	}()

	return splitCtx, files
}

func (s *Splitter) split(ctx context.Context) (int64, string, error) {
	limitedReader := io.LimitReader(s.Src, s.N)
	f, err := ioutil.TempFile(s.Path, s.Prefix)
	if err != nil {
		return 0, "", fmt.Errorf("Can't create tmp file. %s", err)
	}
	defer f.Close()

	// io.EOF on reader is not considered an erro by io.Copy
	n, err := io.Copy(f, limitedReader)

	select {
	case <-ctx.Done():
		defer os.Remove(f.Name())
		return 0, "", fmt.Errorf("Split Operation aborted. %s", ctx.Err())
	default:
		return n, f.Name(), err
	}
}
